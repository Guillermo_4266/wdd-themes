import fnmatch
import glob
import json
import os
import zipfile
from io import BytesIO

from PIL import Image, ImageStat

# List of (Theme Name, Theme Type, Theme Config, Theme Args)
theme_data = []
num_errors = 0
num_warnings = 0

# Ensure output directories are empty
for out_dir in ("app/resources", "downloads", "website/previews", "website/thumbnails"):
    if os.path.isdir(out_dir):
        print("Deleting contents of", out_dir)
        for root, _, files in os.walk(out_dir):
            for filename in files:
                os.remove(os.path.join(root, filename))
    else:
        print("Creating", out_dir)
        os.makedirs(out_dir)

# Load URLs data for external themes
with open("urls.json", 'r') as fileobj:
    urls_data = json.load(fileobj)


def validate_theme_config(theme_config):
    global num_errors
    for key in ("dayImageList", "imageCredits", "imageFilename", "nightImageList", "sunriseImageList", "sunsetImageList"):
        if not theme_config.get(key):
            print(f"Error: {key} not found in theme.json")
            num_errors += 1


def validate_image_size(theme_name, theme_type, theme_config):
    global num_warnings
    image_filename = theme_config["imageFilename"].replace("*", str(theme_config["sunriseImageList"][0]))
    img = Image.open(f"input/{theme_type}/{theme_name}/{image_filename}")
    w, h = img.size

    if h < 1080:
        print(f"Warning: Image size is {w}x{h}")
        num_warnings += 1

    if not (w - 1 <= h * 16 / 9 <= w + 1):
        print(f"Warning: Image ratio is not 16:9")
        num_warnings += 1


def validate_image_brightness(light_image_id, dark_image_id, theme_name, theme_type, theme_config):
    global num_warnings
    image_pattern = f"input/{theme_type}/{theme_name}/" + theme_config["imageFilename"]
    star_lindex = image_pattern.index("*")
    star_rindex = len(image_pattern) - star_lindex - 1
    image_filenames = sorted(glob.glob(image_pattern), key=lambda x: int(x[star_lindex:-star_rindex]))
    image_data = []

    for filename in image_filenames:
        img = Image.open(filename).convert("L")
        stat = ImageStat.Stat(img)
        image_data.append(stat.mean[0])

    lightest_image_id = image_data.index(max(image_data)) + 1
    if not (light_image_id - 1 <= lightest_image_id <= light_image_id + 1):
        print(f"Warning: Lightest image is {lightest_image_id}, expected {light_image_id}")
        num_warnings += 1

    darkest_image_id = image_data.index(min(image_data)) + 1
    if not (dark_image_id - 1 <= darkest_image_id <= dark_image_id + 1):
        print(f"Warning: Darkest image is {darkest_image_id}, expected {dark_image_id}")
        num_warnings += 1


def validate_theme_files(theme_name, theme_type, theme_config):
    global num_warnings
    for filename in os.listdir(f"input/{theme_type}/{theme_name}"):
        basename = os.path.basename(filename)
        if basename != "theme.json" and not fnmatch.fnmatch(basename, theme_config["imageFilename"]):
            print(f"Warning: {basename} will be packaged")
            num_warnings += 1


for theme_type in ("contrib", "default", "extern", "paid"):
    for theme_name in os.listdir(f"input/{theme_type}"):
        print(f"Processing {theme_type}/{theme_name}")

        theme_name = os.path.splitext(theme_name)[0]
        theme_config = {}

        # Load theme config JSON
        if theme_type == "contrib" or theme_type == "default":
            theme_json = f"input/{theme_type}/{theme_name}/theme.json"
            if not os.path.isfile(theme_json):
                print("Error: Missing theme.json")
                num_errors += 1

            with open(theme_json, 'r') as fileobj:
                theme_config = json.load(fileobj)
        elif theme_type == "extern" or theme_type == "paid":
            with zipfile.ZipFile(f"input/{theme_type}/{theme_name}.ddw", 'r') as zipobj:
                with zipobj.open(f"{theme_name}.json", 'r') as fileobj:
                    theme_config = json.load(fileobj)

        current_theme_data = [theme_name, theme_type, theme_config]
        validate_theme_config(theme_config)

        light_image_id = theme_config.get("dayHighlight") or theme_config["dayImageList"][len(theme_config["dayImageList"]) // 2]
        light_image_filename = theme_config["imageFilename"].replace("*", str(light_image_id))
        dark_image_id = theme_config.get("nightHighlight") or theme_config["nightImageList"][len(theme_config["nightImageList"]) // 2]
        dark_image_filename = theme_config["imageFilename"].replace("*", str(dark_image_id))

        if theme_type == "contrib" or theme_type == "default":
            # Ensure Thumbs.db does not exist
            thumbs_db = f"input/{theme_type}/{theme_name}/Thumbs.db"
            if os.path.isfile(thumbs_db):
                os.remove(thumbs_db)

            validate_image_size(*current_theme_data)
            validate_image_brightness(light_image_id, dark_image_id, *current_theme_data)
            validate_theme_files(*current_theme_data)

            # Package .ddw file
            with zipfile.ZipFile(f"downloads/{theme_name}.ddw", 'w', zipfile.ZIP_DEFLATED) as zipobj:
                for filename in os.listdir(f"input/{theme_type}/{theme_name}"):
                    zipobj.write(f"input/{theme_type}/{theme_name}/{filename}", os.path.basename(filename))

        # Generate thumbnail image
        if theme_type == "contrib" or theme_type == "default":
            img1 = Image.open(f"input/{theme_type}/{theme_name}/{light_image_filename}")
            img2 = Image.open(f"input/{theme_type}/{theme_name}/{dark_image_filename}")
        else:
            with zipfile.ZipFile(f"input/{theme_type}/{theme_name}.ddw", 'r') as zipobj:
                img1 = Image.open(BytesIO(zipobj.read(light_image_filename)))
                img2 = Image.open(BytesIO(zipobj.read(dark_image_filename)))

        w, h = img1.size
        img2.paste(img1.crop((0, 0, w // 2, h)))
        img2.thumbnail((w * 216 / h, 216))
        if theme_type != "default":
            img2.save(f"website/thumbnails/{theme_name}_thumbnail.png")
        else:
            img2.save(f"app/resources/{theme_name}_thumbnail.jpg", quality=95)

        # Calculate theme resolution
        theme_resolution = "HD"
        if h >= 3384:
            theme_resolution = "6k"
        elif h >= 2880:
            theme_resolution = "5k"
        elif h >= 2160:
            theme_resolution = "4k"
        elif h >= 1440:
            theme_resolution = "2k"
        current_theme_data.append(theme_resolution)

        if theme_type == "contrib":
            image_paths = []

            # Generate preview images
            for image_id in (theme_config["sunriseImageList"] + theme_config["dayImageList"] + theme_config["sunsetImageList"] + theme_config["nightImageList"]):
                image_filename = theme_config["imageFilename"].replace("*", str(image_id))
                img = Image.open(f"input/{theme_type}/{theme_name}/{image_filename}")
                img.thumbnail((960, 540))
                img.save(f"website/previews/{image_filename}")
                if not (len(image_paths) and image_paths[-1] == f"previews/{image_filename}"):
                    image_paths.append(f"previews/{image_filename}")

            current_theme_data.append(image_paths)
        elif theme_type != "default":
            current_theme_data.append(urls_data[theme_name])

        theme_data.append(current_theme_data)

print("Generating theme data JSON")
themes_free = [td for td in theme_data if td[1] == "contrib" or td[1] == "extern"]
themes_free.sort(key=lambda td: td[0].lower())
themes_paid = [td for td in theme_data if td[1] == "paid"]
themes_paid.sort(key=lambda td: td[0].lower())

for i, theme_data in enumerate(themes_free):
    theme_name, theme_type, theme_config = theme_data[:3]
    if theme_type == "contrib":
        theme_config["fileSize"] = round(os.path.getsize(f"downloads/{theme_name}.ddw") / 1024 / 1024, 2)
    else:
        themes_free[i][2] = {"displayName": theme_config.get("displayName")}

for i, theme_data in enumerate(themes_paid):
    themes_paid[i][2] = {"displayName": themes_paid[i][2].get("displayName")}

with open("website/theme-data.json", 'w') as fileobj:
    json.dump({"free": themes_free, "paid": themes_paid}, fileobj, indent=2)

print(f"Finished with {num_errors} error(s) and {num_warnings} warning(s)")
